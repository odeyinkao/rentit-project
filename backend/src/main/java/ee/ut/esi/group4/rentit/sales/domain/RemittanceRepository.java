package ee.ut.esi.group4.rentit.sales.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RemittanceRepository extends JpaRepository<Remittance, Long> {

}
