package ee.ut.esi.group4.rentit.sales.rest;

import ee.ut.esi.group4.rentit.common.application.exception.PlantNotFoundException;
import ee.ut.esi.group4.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import ee.ut.esi.group4.rentit.inventory.application.service.InventoryService;
import ee.ut.esi.group4.rentit.sales.application.dto.POExtensionDTO;
import ee.ut.esi.group4.rentit.sales.application.dto.PlantInventoryItemDTO;
import ee.ut.esi.group4.rentit.sales.application.dto.PurchaseOrderDTO;
import ee.ut.esi.group4.rentit.sales.application.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/sales")
public class SalesRestController {
    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @GetMapping("/plants")
    @Secured({"ROLE_CLIENT", "ROLE_CLERK"})
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        return inventoryService.findAvailablePlants(plantName.toLowerCase(), startDate, endDate);
    }

    @GetMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    public List<PurchaseOrderDTO> fetchPurchaseOrders()  throws Exception {
        return salesService.loadPurchaseOrder();
    }

    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PurchaseOrderDTO fetchPurchaseOrder(@PathVariable("id") Long id) {
        return salesService.findPO(id);
    }

    @PostMapping("/orders")
    @Secured({"ROLE_CLERK"})
    public PurchaseOrderDTO createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) throws Exception {
         return salesService.createPO(partialPODTO);

    }

    @PostMapping("/orders/{id}/accept")
    @Secured({"ROLE_CLERK"})
    public PurchaseOrderDTO acceptPurchaseOrder(@PathVariable Long id) throws Exception {

        return salesService.acceptPO(id);


    }

    @DeleteMapping("/orders/{id}/reject")
    @Secured({"ROLE_CLERK"})
    public PurchaseOrderDTO rejectPurchaseOrder(@PathVariable Long id) throws Exception {

         return salesService.rejectPO(id);

    }

//    @DeleteMapping("/orders/{id}")
//    @Secured({"ROLE_CLERK"})
//    public PurchaseOrderDTO closePurchaseOrder(@PathVariable Long id) {
//
//         return salesService.rejectPO(id);
//
//    }

    @PutMapping("/orders/{id}")
    @Secured({"ROLE_CLERK"})
    public PurchaseOrderDTO resubmitPurchaseOrder(@PathVariable Long id) throws Exception {

        return salesService.resubmitPO(id);
    }

    @PostMapping("/orders/{id}/extensions")
    @Secured({"ROLE_CLERK"})
    public List<POExtensionDTO> retrievePurchaseOrderExtensions(@PathVariable("id") Long id) {
//        List<EntityModel<POExtensionDTO>> result = new ArrayList<>();
//        POExtensionDTO extension = new POExtensionDTO();
//        extension.setEndDate(LocalDate.now().plusWeeks(1));

//        result.add(new EntityModel<>(extension));
//        return new CollectionModel<>(result,
//                linkTo(methodOn(SalesRestController.class).retrievePurchaseOrderExtensions(id))
//                        .withSelfRel()
//                        .andAffordance(afford(methodOn(SalesRestController.class).requestPurchaseOrderExtension(null, id))));

        return null;
    }

    @PostMapping("/orders/{id}/extension/accept")
    @Secured({"ROLE_CLERK"})
    public PurchaseOrderDTO acceptPurchaseOrderExtension(@PathVariable Long id) throws Exception {

        return salesService.acceptPOExtension(id);


    }

    @DeleteMapping("/orders/{id}/extension/reject")
    @Secured({"ROLE_CLERK"})
    public PurchaseOrderDTO rejectPurchaseOrderExtension(@PathVariable Long id) throws Exception {

        return salesService.rejectPOExtension(id);

    }

    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handlePlantNotFoundException(PlantNotFoundException ex) {
    }

    @PostMapping("/plants/{id}/returned")
    public PlantInventoryItemDTO returnPlant(@PathVariable Long id) throws Exception {

        return salesService.returnPlant(id);
    }


    @PostMapping("/plants/{id}/dispatched")
    public PlantInventoryItemDTO dispatchPlant(@PathVariable Long id) throws Exception {
        return salesService.dispatchPlant(id);
    }

    @GetMapping("/plants/listing")
    public List<PlantInventoryItemDTO> loadPlants() throws Exception{
        return salesService.loadPlants();
    }

    @PostMapping("/orders/{id}/invoice/submit")
    public PurchaseOrderDTO submitInvoiceforReturnedPO(@PathVariable Long id){

        return salesService.submitInvoiceforReturnedPO(id);
    }

}