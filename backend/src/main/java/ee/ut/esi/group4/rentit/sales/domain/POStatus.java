package ee.ut.esi.group4.rentit.sales.domain;

public enum POStatus {
    PENDING, REJECTED, OPEN, CLOSED, INVOICED,  PENDING_EXTENSION,  REJECT_EXTENSION
}
