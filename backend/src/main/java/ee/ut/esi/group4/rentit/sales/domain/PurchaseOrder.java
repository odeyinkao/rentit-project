package ee.ut.esi.group4.rentit.sales.domain;

import ee.ut.esi.group4.rentit.common.domain.BusinessPeriod;
import ee.ut.esi.group4.rentit.inventory.domain.model.PlantInventoryEntry;
import ee.ut.esi.group4.rentit.inventory.domain.model.PlantReservation;
import ee.ut.esi.group4.rentit.inventory.domain.model.StatusMessagingData;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PurchaseOrder {
    @Id
    @GeneratedValue
    Long id;

    @OneToMany
    List<PlantReservation> reservations;

    @ManyToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;
    LocalDate paymentSchedule;

    @Column(precision=8,scale=2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;



    @Embedded
    BusinessPeriod rentalPeriod;

    @OneToOne(optional = true, cascade = CascadeType.PERSIST)
    POExtension extension;

    @OneToOne(optional = true, cascade = CascadeType.PERSIST)
    Invoice invoice;

    @OneToOne(optional = true, cascade = CascadeType.PERSIST)
    Remittance remittance;

    String createdby;

    @ElementCollection
    List<StatusMessagingData> messages = new ArrayList<>();

    public static PurchaseOrder of(PlantInventoryEntry entry, BusinessPeriod period) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = entry;
        po.rentalPeriod = period;
        po.reservations = new ArrayList<>();
        po.issueDate = LocalDate.now();
        po.status = POStatus.PENDING;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        po.createdby  = authentication.getName();

        return po;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public void setStatus(POStatus newStatus) {
        this.status = newStatus;
    }

    public void addReservation(PlantReservation reservation) {
        reservations.add(reservation);
    }

    public void setInvoice (Invoice invoice){
        this.invoice = invoice;
    }

    public void setRemittance (Remittance remittance){
        this.remittance = remittance;
    }

//    public void requestExtension(POExtension extension) {
//        this.extension = extension;
//        this.status = POStatus.PENDING_EXTENSION;
//    }

    public void setPOExtension(POExtension ext) {
        this.extension = ext;
    }

    public void setExtension (POExtension extension){
        this.extension = extension;
    }

    public void setRentalPeriod(BusinessPeriod period) {

        this.rentalPeriod = period;
    }

//    public void setLocation(PlantLocation location){
//        this.location =  location;
//    }

//    public LocalDate pendingExtensionEndDate() {
//        if (extensions.size() > 0) {
//            POExtension openExtension = extensions.get(extensions.size() - 1);
//            return openExtension.getEndDate();
//        }
//        return null;
//    }

//    public void acceptExtension(PlantReservation reservation) {
//        this.reservations.add(reservation);
//        this.status = POStatus.OPEN;
//        this.rentalPeriod = BusinessPeriod.of(rentalPeriod.getStartDate(), reservation.getSchedule().getEndDate());
//    }

    public void addStatusMessage(StatusMessagingData message) {
        this.messages.add(message);
    }
}
