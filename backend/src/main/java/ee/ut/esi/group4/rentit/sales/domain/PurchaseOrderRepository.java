package ee.ut.esi.group4.rentit.sales.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {

    @Query("select p from PurchaseOrder p where LOWER(p.createdby) = ?1")
    List<PurchaseOrder> findByCreator(String createdby);

    @Query("select p from PurchaseOrder p where LOWER(p.createdby) = ?1 and p.id = ?2")
    Optional<PurchaseOrder> findByOwnerAndPoId(String owner, Long entityId);

//    @Query("select p from PurchaseOrder p where p.plant = ?1 and p.id = ?2")
//    Optional<PurchaseOrder> findByPlantAndStatus(PlantInventoryItem pi, );
}
