package ee.ut.esi.group4.rentit.sales.domain;

import ee.ut.esi.group4.rentit.inventory.domain.model.PlantReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends JpaRepository<PlantReservation, Long> {

}
