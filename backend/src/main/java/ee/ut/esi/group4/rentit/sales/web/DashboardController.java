package ee.ut.esi.group4.rentit.sales.web;

import ee.ut.esi.group4.rentit.inventory.application.service.InventoryService;
import ee.ut.esi.group4.rentit.sales.application.dto.CatalogQueryDTO;
import ee.ut.esi.group4.rentit.sales.application.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dashboard")
public class	DashboardController	{
    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @GetMapping("/catalog/form")
    public String getQueryForm(Model model)	{
        model.addAttribute("catalogQuery", new CatalogQueryDTO());
        return	"dashboard/catalog/query-form";
    }

//    @PostMapping("/catalog/query")
//    public String getQueryResult(CatalogQueryDTO query, Model model) throws Exception {
//        List<PlantInventoryEntryDTO> plants = inventoryService.findAvailablePlants(query.getName().toLowerCase(),
//                query.getRentalPeriod().getStartDate(),
//                query.getRentalPeriod().getEndDate());
//
//        model.addAttribute("plants", plants);
//
//        PurchaseOrderDTO po = new PurchaseOrderDTO();
//        po.setRentalPeriod(query.getRentalPeriod());
//        if(!plants.isEmpty()) {
//            po.setPlant(plants.get(0));
//            salesService.createPO(po);
//        }
//
//        model.addAttribute("po", po);
//
//        return	"dashboard/catalog/query-result";
//    }

}